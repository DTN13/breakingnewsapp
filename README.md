# BreakingNewsApp

Cette application récupère les dernière actualités et les affiche directement à l'écran (et
accessoirement m'aide à m'améliorer en `Kotlin`). N'hésitez pas à me donner vos remarques, ainsi
vous m'aiderez à progresser ! ;)

## Injection de dépendance

`Koin` est utilisé pour gérer l'injection des dépendances classiques (API, Repository, ViewModel).
Le choix entre `Hilt` et Koin est complètement arbitraire, tous les 2 étant des très bonnes
libraries. La plus grande différence serait que `Hilt` construit les dépendances dès la compilation
contrairement à `Koin` qui le fait que lorsque les dépendances en question allaient être utilisées.

## Structure de l'application

[MVVM](architecture.png) : Architecture classique à `Android` et qui fonctionne bien pour séparer le
code. J'aurais pu abstraire le Repository, ainsi isoler encore plus la partie données externes qui
ne serait qu'une implémentation et ajouter des Use Cases (`Clean Architecture`) mais l'intérêt est
minime dans cette petite application qui n'a qu'une use case de charger les news.

## Authentification API_KEY

Vous aurez besoin d'une `API_KEY` que vous pouvez obtenir [ici](https://newsapi.org/account). (Vous
pouvez aussi me contacter à duytinh.nguyen@live.fr si vous ne voulez pas créer de compte)

## Networking

`Retrofit` et `OkHttpClient` sont choisis pour la partie Networking : choix unanime je dirais.

## UI avec Jetpack Compose

- `Coroutine` support
- Accélère le développement avec le live preview contrairement à XML
- Notion de State et Recomposition
- 1ère version stable depuis 1 an

## Unit Test

Deux tests sont écrits pour l'instant:

- [APITest](app/src/test/java/com/tinhcompany/breakingnews/APITest.kt) : Permet de vérifier le que
  la classe [NewsAPI](app/src/main/java/com/tinhcompany/breakingnews/api/NewsAPI.kt) fonctionne
  bien.
- [NewsPagingSourceTest](app/src/test/java/com/tinhcompany/breakingnews/NewsPagingSourceTest.kt) :
  Permet de vérifier le fonctionnement
  de [NewsPagingSource](app/src/main/java/com/tinhcompany/breakingnews/repository/NewsPagingSource.kt)

Il manque notamment tous les tests sur l'UI, sur le ViewModel, et bien d'autres (modules koin, api
endpoints, etc)  
