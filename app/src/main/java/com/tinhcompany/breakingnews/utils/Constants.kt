package com.tinhcompany.breakingnews.utils

class Constants {

    companion object {
        const val BASE_URL = "https://newsapi.org"
        const val QUERY_PAGE_SIZE = 20
    }
}