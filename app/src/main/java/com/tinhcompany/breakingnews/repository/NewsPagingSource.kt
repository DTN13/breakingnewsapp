package com.tinhcompany.breakingnews.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.tinhcompany.breakingnews.api.NewsAPI
import com.tinhcompany.breakingnews.models.Article
import retrofit2.HttpException
import java.io.IOException

const val STARTING_PAGE_INDEX = 1
const val PAGE_SIZE = 25

class NewsPagingSource(
    private val api: NewsAPI
) : PagingSource<Int, Article>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Article> {
        val pageIndex = params.key ?: STARTING_PAGE_INDEX
        return try {
            val newsResponse = api.getBreakingNews(
                pageNumber = pageIndex
            )

            val articles = newsResponse.articles
            val nextKey =
                if (articles.isEmpty()) {
                    null
                } else {
                    pageIndex + (params.loadSize / PAGE_SIZE)
                }

            LoadResult.Page(
                data = newsResponse.articles,
                prevKey = if (pageIndex == STARTING_PAGE_INDEX) null else pageIndex,
                nextKey = nextKey
            )

        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    /**
     * The refresh key is used for subsequent calls to PagingSource.Load after the initial load.
     */
    override fun getRefreshKey(state: PagingState<Int, Article>): Int? {
        // We need to get the previous key (or next key if previous is null) of the page
        // that was closest to the most recently accessed index.
        // Anchor position is the most recently accessed index.
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}