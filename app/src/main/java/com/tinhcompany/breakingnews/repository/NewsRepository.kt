package com.tinhcompany.breakingnews.repository

import com.tinhcompany.breakingnews.api.NewsAPI

open class NewsRepository(
    private val api: NewsAPI
) {
    open fun getNewsPagingSource() = NewsPagingSource(api)
}