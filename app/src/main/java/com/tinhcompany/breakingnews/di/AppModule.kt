package com.tinhcompany.breakingnews.di

import com.tinhcompany.breakingnews.api.NewsAPI
import com.tinhcompany.breakingnews.repository.NewsRepository
import com.tinhcompany.breakingnews.ui.NewsViewModel
import com.tinhcompany.breakingnews.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {
    single {
        Retrofit.Builder()
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    })
                    .build()
            )             //classic client, could add interceptor to debug body content, add header, or add ssl certificate if server requires
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NewsAPI::class.java)
    }

    single {
        NewsRepository(get())
    }

    viewModel {
        NewsViewModel(get())
    }
}