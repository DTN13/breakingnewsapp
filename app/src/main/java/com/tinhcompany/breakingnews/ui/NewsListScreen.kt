package com.tinhcompany.breakingnews.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import coil.compose.AsyncImage
import com.tinhcompany.breakingnews.models.Article
import de.charlex.compose.HtmlText
import kotlinx.coroutines.flow.Flow

@Composable
fun NewsListScreen(
    viewModel: NewsViewModel,
    navController: NavController
) {
    NewsList(
        viewModel = viewModel,
        navController = navController
    )
}

@Composable
fun ArticleItem(article: Article, onClick: () -> Unit) {
    Card(
        modifier = Modifier
            .padding(
                bottom = 5.dp, top = 5.dp,
                start = 5.dp, end = 5.dp
            )
            .fillMaxWidth()
            .clickable(onClick = onClick),
        shape = RoundedCornerShape(15.dp),
        elevation = 12.dp
    ) {
        Row(
            modifier = Modifier
                .clip(RoundedCornerShape(4.dp))
                .background(MaterialTheme.colors.surface)
        ) {
            Surface(
                modifier = Modifier.size(130.dp),
                shape = RoundedCornerShape(12.dp),
                color = MaterialTheme.colors.surface.copy(
                    alpha = 0.2f
                )
            ) {
                AsyncImage(
                    model = article.urlToImage,
                    contentDescription = null,
                    modifier = Modifier
                        .height(100.dp)
                        .clip(shape = RoundedCornerShape(12.dp)),
                    contentScale = ContentScale.Crop
                )
            }
            Column(
                modifier = Modifier
                    .padding(start = 12.dp, top = 12.dp)
                    .align(Alignment.Top)
            ) {
                Text(
                    text = article.title ?: "",
                    fontWeight = FontWeight.Bold,
                    style = TextStyle(fontSize = 17.sp),
                    color = Color.Black,
                    maxLines = 2,
                )
                HtmlText(
                    modifier = Modifier.padding(top = 5.dp),
                    text = article.description ?: "",
                    fontSize = 12.sp,
                    fontStyle = FontStyle.Italic,
                    color = Color.DarkGray,
                    maxLines = 3
                )
            }
        }
    }
}

@Composable
fun NewsList(viewModel: NewsViewModel, navController: NavController) {
    NewsList(
        newsList = viewModel.news,
        viewModel = viewModel,
        navController = navController
    )
}

@Composable
fun NewsList(
    newsList: Flow<PagingData<Article>>,
    viewModel: NewsViewModel,
    navController: NavController
) {
    val newsPagingItem: LazyPagingItems<Article> = newsList.collectAsLazyPagingItems()

    LazyColumn {
        items(newsPagingItem) { article ->
            article?.let {
                ArticleItem(
                    article = it,
                    onClick = {
                        viewModel.selectedArticle = it
                        navController.navigate(
                            route = Screen.NewsDetailScreen.route
                        )
                    })
            }
        }

        newsPagingItem.apply {
            if (loadState.refresh is LoadState.Error || loadState.append is LoadState.Error) {
                val error = if (loadState.refresh is LoadState.Error) {
                    loadState.refresh as LoadState.Error
                } else {
                    loadState.append as LoadState.Error
                }
                item { ErrorText(error.error.message ?: "") }
            }

            if (loadState.refresh is LoadState.Loading || loadState.append is LoadState.Loading) {
                item { Loading() }
            }
        }

    }
}

@Composable
fun ErrorText(text: String) {
    val end =
        if (text.isBlank())
            "."
        else
            ": $text"

    Text(
        text = "Sorry, something's wrong$end",
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally)
            .padding(top = 5.dp, bottom = 5.dp)
    )
}

@Composable
fun Loading() {
    CircularProgressIndicator(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally)
    )
}


