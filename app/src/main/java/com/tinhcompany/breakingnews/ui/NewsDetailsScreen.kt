package com.tinhcompany.breakingnews.ui

import android.content.Context
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import coil.compose.AsyncImage
import de.charlex.compose.HtmlText

@Composable
fun NewsDetailScreen(
    viewModel: NewsViewModel
) {
    val article = viewModel.selectedArticle

    Column(
        modifier = Modifier
            .scrollable(
                enabled = true,
                state = rememberScrollState(),
                orientation = Orientation.Vertical
            )
            .fillMaxSize()
            .padding(start = 7.dp, end = 7.dp)
    ) {
        //Image
        Surface(
            modifier = Modifier
                .padding(all = 10.dp)
                .align(alignment = Alignment.CenterHorizontally),

            shape = RoundedCornerShape(12.dp),
            color = MaterialTheme.colors.surface.copy(
                alpha = 0.2f
            ),
        ) {
            AsyncImage(
                model = article?.urlToImage,
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.3f),
                contentScale = ContentScale.Crop
            )
        }

        //Title
        Text(
            text = article?.title ?: "",
            fontWeight = FontWeight.Bold,
            style = TextStyle(fontSize = 33.sp),
            color = Color.Black,
        )

        //URL
        val annotatedArticleUrl = annotatedString(
            text = "Lire l'article en entier",
            url = article?.url ?: ""
        )
        val uriHandler = LocalUriHandler.current
        ClickableText(
            modifier = Modifier
                .padding(10.dp)
                .fillMaxWidth(),
            text = annotatedArticleUrl,
            onClick = {
                annotatedArticleUrl
                    .getStringAnnotations("URL", it, it)
                    .firstOrNull()?.let { stringAnnotation ->
                        uriHandler.openUri(stringAnnotation.item)
                    }
            }
        )

        //Content
        HtmlText(
            modifier = Modifier
                .padding(top = 5.dp),
            text = article?.content ?: "",
            fontSize = 22.sp,
            color = Color.DarkGray,
        )
    }
}

fun annotatedString(text: String, url: String): AnnotatedString {
    val startIndex = 0
    val endIndex = text.length

    return buildAnnotatedString {
        append(text = text)
        addStyle(
            style = SpanStyle(
                color = Color.Blue,
                fontSize = 18.sp,
                textDecoration = TextDecoration.Underline,
                fontStyle = FontStyle.Italic
            ),
            start = startIndex,
            end = endIndex
        )
        addStringAnnotation(
            tag = "URL",
            annotation = url,
            start = startIndex,
            end = endIndex,
        )
    }
}

@Composable
fun WebViewCompose(context: Context, url: String) {

    AndroidView(factory = {
        WebView(context).apply {
            webViewClient = WebViewClient()
            loadUrl(url)
        }
    })
}