package com.tinhcompany.breakingnews.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.tinhcompany.breakingnews.models.Article
import com.tinhcompany.breakingnews.repository.NewsRepository
import com.tinhcompany.breakingnews.repository.PAGE_SIZE
import kotlinx.coroutines.flow.Flow

class NewsViewModel(
    private val repository: NewsRepository
) : ViewModel() {

    val news: Flow<PagingData<Article>> = Pager(
        config = PagingConfig(pageSize = PAGE_SIZE),
        pagingSourceFactory = { repository.getNewsPagingSource() }
    )
        .flow
        .cachedIn(viewModelScope)

    var selectedArticle by mutableStateOf<Article?>(value = null)
}
