package com.tinhcompany.breakingnews.ui

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.Surface
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import org.koin.android.ext.android.inject

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Surface {
                val navController = rememberNavController()
                val newsViewModel by inject<NewsViewModel>()

                NavHost(
                    navController = navController,
                    startDestination = Screen.NewsListScreen.route
                ) {
                    composable(route = Screen.NewsListScreen.route) {
                        NewsListScreen(
                            viewModel = newsViewModel,
                            navController = navController
                        )
                    }
                    composable(route = Screen.NewsDetailScreen.route) {
                        NewsDetailScreen(
                            viewModel = newsViewModel
                        )
                    }
                }
            }
        }
    }
}