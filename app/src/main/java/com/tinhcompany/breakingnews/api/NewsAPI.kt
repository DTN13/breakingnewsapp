package com.tinhcompany.breakingnews.api

import com.tinhcompany.breakingnews.BuildConfig
import com.tinhcompany.breakingnews.models.NewsResponse
import com.tinhcompany.breakingnews.repository.PAGE_SIZE
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsAPI {

    @GET("v2/top-headlines")
    suspend fun getBreakingNews(
        @Query("country")
        country: String = "fr",
        @Query("page_size")
        pageSize: Int = PAGE_SIZE,
        @Query("page")
        pageNumber: Int = 1,
        @Query("apiKey")
        apiKey: String = BuildConfig.API_KEY
    ): NewsResponse
}