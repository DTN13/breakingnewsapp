package com.tinhcompany.breakingnews.models

import com.google.gson.annotations.SerializedName
import com.tinhcompany.breakingnews.repository.STARTING_PAGE_INDEX

data class NewsResponse(

    @SerializedName("articles")
    val articles: MutableList<Article>
)
