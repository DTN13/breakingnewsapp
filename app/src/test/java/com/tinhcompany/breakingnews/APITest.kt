package com.tinhcompany.breakingnews

import androidx.paging.PagingSource
import com.tinhcompany.breakingnews.api.NewsAPI
import com.tinhcompany.breakingnews.repository.NewsRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class APITest {

    val testBodyJson: String =
        "{\"status\":\"ok\",\"totalResults\":4,\"articles\":[{\"source\":{\"id\":\"bbc-news\",\"name\":\"BBC News\"},\"author\":\"https://www.facebook.com/bbcnews\",\"title\":\"Lord Geidt quit over ministerial code 'affront'\",\"description\":\"PM's former ethics adviser Lord Geidt says he quit after being placed in “impossible and odious” position over plan to risk breaking ministerial code\",\"url\":\"https://www.bbc.co.uk/news/uk-politics-61827319\",\"urlToImage\":\"https://ichef.bbci.co.uk/news/1024/branded_news/83B3/production/_115651733_breaking-large-promo-nc.png\",\"publishedAt\":\"2022-06-16T10:57:11Z\",\"content\":\"Boris Johnson's former ethics adviser Lord Geidt says he quit after being placed in an impossible and odious position over a plan to risk breaking the ministerial code.\\r\\nIn his resignation letter, he… [+451 chars]\"},{\"source\":{\"id\":null,\"name\":\"The Guardian\"},\"author\":\"Stuart Heritage\",\"title\":\"Johnson’s resignation – and other great moments in breaking news\",\"description\":\"After the PM stood down virtually live on Radio 4, we interrupt this broadcast to bring you some jarring tonal shifts, the best putdowns of Simon McCoy, and a message from outer spaceAfter a shambolic and ad-hoc premiership, it’s fitting that Boris Johnson’s …\",\"url\":\"https://amp.theguardian.com/tv-and-radio/2022/jul/07/johnson-resignation-breaking-news-simon-mccoy\",\"urlToImage\":\"https://i.guim.co.uk/img/media/a0903f3dc753d8d1fc728707a888ba83585167b4/2404_712_2722_1634/master/2722.jpg?width=1200&height=630&quality=85&auto=format&fit=crop&overlay-align=bottom%2Cleft&overlay-width=100p&overlay-base64=L2ltZy9zdGF0aWMvb3ZlcmxheXMvdGctZGVmYXVsdC5wbmc&enable=upscale&s=600188fd2a1b5ee5558779c89275a8c1\",\"publishedAt\":\"2022-07-07T14:25:59Z\",\"content\":\"After a shambolic and ad-hoc premiership, its fitting that Boris Johnsons time in power crashed to a halt in such a chaotic and ad-hoc way. During this mornings Today show on Radio 4, BBC political e… [+3953 chars]\"},{\"source\":{\"id\":\"bbc-news\",\"name\":\"BBC News\"},\"author\":\"https://www.facebook.com/bbcnews\",\"title\":\"Living through Japan's hottest summer on record\",\"description\":\"Japan is experiencing record-breaking heat - and summer has hardly begun.\",\"url\":\"https://www.bbc.co.uk/news/world-asia-61990913\",\"urlToImage\":\"https://ichef.bbci.co.uk/news/1024/branded_news/142C6/production/_125703628_gettyimages-1241562260.jpg\",\"publishedAt\":\"2022-06-30T23:23:34Z\",\"content\":\"By Rupert Wingfield-Hayes``BBC News, Tokyo\\r\\nMasaya Maruyama, his wife Junko and his brother Koichi, were sitting under the tailgate of their work van, trying to find a tiny bit of shade, as they dran… [+2953 chars]\"},{\"source\":{\"id\":null,\"name\":\"CP24 Toronto's Breaking News\"},\"author\":\"Joshua Freeman\",\"title\":\"Special air quality statement in effect for Toronto amid record-breaking heat and high pollution - CP24 Toronto's Breaking News\",\"description\":\"<ol><li>Special air quality statement in effect for Toronto amid record-breaking heat and high pollution  CP24 Toronto's Breaking News\\r\\n</li><li>BlackburnNews.com - Midwestern Ontario under heat warning  BlackburnNews.com\\r\\n</li><li>Heat warning continues for …\",\"url\":\"https://www.cp24.com/news/special-air-quality-statement-in-effect-for-toronto-amid-record-breaking-heat-and-high-pollution-1.5957642\",\"urlToImage\":\"https://www.cp24.com/polopoly_fs/1.2441721.1442835445!/httpImage/image.jpg_gen/derivatives/landscape_620/image.jpg\",\"publishedAt\":\"2022-06-22T03:29:42Z\",\"content\":\"Environment Canada has issued a special air quality statement for Toronto and the rest of the GTA as hot and humid weather conditions create elevated pollution levels.\\r\\nIn a statement issued late Tue… [+1471 chars]\"}]}"

    private val mockWebServer = MockWebServer()

    private val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(NewsAPI::class.java)

    private val newsRepository = NewsRepository(api)

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun testSuccessful() {
        val body = testBodyJson
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(body)
        )

        runBlocking {
            val test = newsRepository.getNewsPagingSource().load(
                PagingSource.LoadParams.Refresh(
                    key = null,
                    loadSize = 2,
                    placeholdersEnabled = false
                )
            )
            assert(test is PagingSource.LoadResult.Page)
        }
    }

    @Test
    fun testHaveResults() {
        val body = testBodyJson
        mockWebServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(body)
        )

        runBlocking {
            val test = newsRepository.getNewsPagingSource().load(
                PagingSource.LoadParams.Refresh(
                    key = null,
                    loadSize = 2,
                    placeholdersEnabled = false
                )
            )
            assert(test is PagingSource.LoadResult.Page)
            assert((test as PagingSource.LoadResult.Page).data.isNotEmpty())
        }
    }
}