package com.tinhcompany.breakingnews

import androidx.paging.PagingSource
import com.tinhcompany.breakingnews.models.Article
import com.tinhcompany.breakingnews.repository.NewsPagingSource
import kotlinx.coroutines.test.runTest
import org.junit.Test
import kotlin.test.assertEquals

class NewsPagingSourceTest {
    private val newsFactory = NewsFactory()
    private val fakeNewsList = listOf(
        newsFactory.createNewsArticle(),
        newsFactory.createNewsArticle(),
        newsFactory.createNewsArticle()
    )
    private val fakeApi = FakeNewsApi().apply {
        fakeNewsList.forEach { article -> addArticle(article) }
    }

    @Test
    fun itemKeyedSubredditPagingSource() = runTest {
        val pagingSource = NewsPagingSource(fakeApi)
        assertEquals(
            expected = PagingSource.LoadResult.Page(
                data = fakeNewsList,
                prevKey = null,
                nextKey = 1
            ),
            actual = pagingSource.load(
                PagingSource.LoadParams.Refresh(
                    key = null,
                    loadSize = 2,
                    placeholdersEnabled = false
                )
            )
        )
    }
}