package com.tinhcompany.breakingnews

import com.tinhcompany.breakingnews.api.NewsAPI
import com.tinhcompany.breakingnews.models.Article
import com.tinhcompany.breakingnews.models.NewsResponse

class FakeNewsApi : NewsAPI {

    private val fakeNewsList = mutableListOf<Article>()

    override suspend fun getBreakingNews(
        country: String,
        pageSize: Int,
        pageNumber: Int,
        apiKey: String
    ): NewsResponse {
        val items = getNewsList()
        return NewsResponse(
            articles = items
        )
    }

    fun addArticle(article: Article) {
        fakeNewsList.add(article)
    }

    fun getNewsList(): MutableList<Article> = fakeNewsList
}