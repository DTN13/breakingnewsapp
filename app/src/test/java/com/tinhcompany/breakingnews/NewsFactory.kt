package com.tinhcompany.breakingnews

import com.tinhcompany.breakingnews.models.Article
import java.util.concurrent.atomic.AtomicInteger

class NewsFactory {
    private val counter = AtomicInteger(0)
    fun createNewsArticle(): Article {
        val id = counter.incrementAndGet()
        val article = Article(
            title = "Breaking news title $id",
            url = "url_$id",
            description = "Description $id",
            urlToImage = null,
            content = "This is the content of news $id"
        )
        return article
    }
}